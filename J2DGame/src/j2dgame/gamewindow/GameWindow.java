/*
 * Copyright (c) 2015, Michael Goodwin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package j2dgame.gamewindow;

import j2dgame.component.RenderComponent;
import j2dgame.component.Renderable;
import j2dgame.component.ScreenRenderComponent;
import j2dgame.gameobjects.GameObject;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;

/**
 *
 * @author Michael Goodwin
 */
public class GameWindow extends Canvas implements Renderable {

    RenderComponent renderComponent = new ScreenRenderComponent();
    private BufferedImage image;
    private int[] pixels;
    private Random random;

    public GameWindow(int width, int height, int scale) {

        setPreferredSize(new Dimension(width * scale, height * scale));
        setMaximumSize(new Dimension(width * scale, height * scale));
        setMinimumSize(new Dimension(width * scale, height * scale));
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        random = new Random();
    }

    public void update() {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = random.nextInt();
        }
    }

    @Override
    public void render() {
        renderComponent.render(this);
    }

    public Image getImage() {
        return image;
    }

}
