package test.gfx.rendering;

import java.awt.Graphics;

import test.object.IGObject;

public interface Renderable {
	public void render();

	public IGObject setGraphics(Graphics graphics);
}
