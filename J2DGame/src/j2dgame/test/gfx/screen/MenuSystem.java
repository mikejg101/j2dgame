package test.gfx.screen;

import java.awt.Graphics;

import test.object.IGObject;
import test.object.viewobject.ScreenObject;

public class MenuSystem implements IGObject {

	private ScreenObject topMenu;
	private ScreenObject bottomMenu;
	private Graphics graphics;
	private int menuRows = 20;
	private int offset = 10;

	public MenuSystem(int width, int height, int scale) {
		topMenu = new ScreenObject(width, 20, scale);
		bottomMenu = new ScreenObject(width, 20, scale);
		topMenu.setRenderPosition(0, 0, width, menuRows);
		bottomMenu.setRenderPosition(0, (height - menuRows) - (offset - 1), width, menuRows);
	}

	@Override
	public void render() {
		topMenu.setGraphics(graphics).render();
		bottomMenu.setGraphics(graphics).render();
	}

	@Override
	public void update() {
		topMenu.update();
		bottomMenu.update();
	}

	@Override
	public IGObject setGraphics(Graphics graphics) {
		this.graphics = graphics;
		return this;
	}

	public IGObject incrementCount() {
		topMenu.incrementCount();
		bottomMenu.incrementCount();
		return this;
	}

}
