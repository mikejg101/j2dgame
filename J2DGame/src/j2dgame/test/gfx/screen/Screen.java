package test.gfx.screen;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public final class Screen extends Canvas {

	private final JFrame frame;
	private final Dimension size;
	private final Dimension scaledSize;

	/**
	 * 
	 */
	private static final long serialVersionUID = -1692148783584825030L;

	public Screen(int width, int height, int scale) {
		this.frame = new JFrame();
		this.size = new Dimension(width, height);
		this.scaledSize = new Dimension(width * scale, height * scale);
		this.setSize(size);
		this.frame.setPreferredSize(new Dimension(scaledSize));
		this.frame.setMaximumSize(new Dimension(scaledSize));
		this.frame.setMinimumSize(new Dimension(scaledSize));
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLocationRelativeTo(null);
		this.frame.setLayout(new BorderLayout());
		this.frame.setResizable(false);
		this.frame.add(this, BorderLayout.CENTER);
		this.frame.pack();
		this.frame.setVisible(true);
	}

}