package test;

public interface Updateable {
	public void update();

	public Updateable incrementCount();
}
