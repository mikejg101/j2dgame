package test.debug;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.PrintStream;

public class Debug {

	public static boolean DEBUG_MODE = false;

	public static void print(PrintStream stream, String... content) {
		StringBuilder message = new StringBuilder();
		for (int i = 0; i < content.length; i++) {
			message.append(content[i]);
		}
		stream.println(message.toString());
	}

	public static void showFrames(Graphics graphics, String message) {
		graphics.setColor(Color.BLACK);
		graphics.fillRect(0, 0, 160 * 3, (25 + 10));
		graphics.fillRect(0, (160 * 3) - (25 * 3), 160 * 3, 160 * 3);
		graphics.setColor(Color.WHITE);
		graphics.setFont(new Font("Arial", Font.BOLD, 25));
		graphics.drawString("Debug Mode", 10, 25);
		graphics.drawString(message, 10, ((160 * 3) - (25 + 10)));
	}
}
