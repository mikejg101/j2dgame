package test.object.viewobject;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import test.object.IGObject;

public abstract class ViewObject implements IGObject {

	private Graphics graphics;
	private final BufferedImage image;
	private int[] pixels;
	private int scale;
	private int xStart = 0;
	private int yStart = 0;
	private int xEnd = 0;
	private int yEnd = 0;

	public ViewObject(int width, int height, int scale) {
		this.scale = scale;
		this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		this.pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	}

	public final void setRenderPosition(int xStart, int yStart, int xEnd, int yEnd) {
		this.xStart = xStart * scale;
		this.yStart = yStart * scale;
		this.xEnd = xEnd * scale;
		this.yEnd = yEnd * scale;
	}

	public final int[] getPixels() {
		return pixels;
	}

	public final IGObject setGraphics(Graphics g) {
		this.graphics = g;
		return this;
	}

	public final Graphics getGraphics() {
		return graphics;
	}

	public final BufferedImage getImage() {
		return image;
	}

	public final int getXStart() {
		return xStart;
	}

	public final int getXEnd() {
		return xEnd;
	}

	public final int getYStart() {
		return yStart;
	}

	public final int getYEnd() {
		return yEnd;
	}

}
