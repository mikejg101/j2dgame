package test.object.viewobject;

import test.object.IGObject;

public class ScreenObject extends ViewObject {

	private int updateCount = 0;

	public ScreenObject(int width, int height, int scale) {
		super(width, height, scale);
	}

	@Override
	public void render() {
		getGraphics().drawImage(getImage(), getXStart(), getYStart(), getXEnd(), getYEnd(), null);
	}

	@Override
	public void update() {
		for (int i = 0; i < getPixels().length; i++) {
			getPixels()[i] = i + updateCount;
		}
	}

	public IGObject incrementCount() {
		this.updateCount++;
		return this;
	}

	public final int getUpdateCount() {
		return updateCount;
	}

}
