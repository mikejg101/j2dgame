package test.object;

import test.Updateable;
import test.gfx.rendering.Renderable;

public interface IGObject extends Renderable, Updateable{

}
