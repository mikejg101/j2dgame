package j2dgame.test;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import test.debug.Debug;
import test.gfx.screen.MenuSystem;
import test.gfx.screen.Screen;
import test.object.IGObject;
import test.object.viewobject.ScreenObject;

public class G implements Runnable {
	private static final double NS_PER_UPDATE = Math.pow(10, 9) / 60D;

	private boolean isRunning = false; // Loop control
	private int updates = 0; // Count the number of updates
	private int frames = 0; // Count the number of frames
	private Screen screen; // User interface
	private MenuSystem menuSystem; // User Menu System
	private ScreenObject content;
	private ArrayList<IGObject> objects; // List of IGObjects
	private String message = "Frame: " + frames + ", Update: " + updates;

	public G(Screen ui, int width, int height, int scale) {
		this.setUi(ui);
		this.objects = new ArrayList<IGObject>();
		this.menuSystem = new MenuSystem(width, height, scale);
		this.content = new ScreenObject(width, height, scale) {
			@Override
			public void update() {
				for (int i = 0; i < this.getPixels().length; i++) {
					this.getPixels()[i] = i - this.getUpdateCount();
				}
			}
		};
		this.content.setRenderPosition(0, 0, width, height);
		this.objects.add(content);
		this.objects.add(menuSystem);
	}

	public synchronized void start() {
		this.isRunning = true;
		this.screen.setVisible(isRunning);
		new Thread(this).start();
	}

	public synchronized void stop() {
		this.isRunning = false;
	}

	@Override
	public void run() {

		// Get the time when the run method was initially called.
		long start = System.nanoTime();

		// Create a delta variable to record the difference between the time at
		// the start of the loop and the current loop time.
		double delta = 0.0;

		// Create a variable to hold the time when the loop starts
		long now = 0;

		// This timer is used to count seconds. This will calculate frames and
		// updates per second.
		long lastTimer = System.currentTimeMillis();

		// Control for rendering
		boolean shouldRender;

		// Run the loop if the application is running
		while (isRunning) {

			// Prevent rendering unless the application has updated.
			shouldRender = false;

			// Get the current time at the beginning of the loop
			now = System.nanoTime();

			// Get the delta and convert.
			delta += (now - start);

			// Set the start variable
			start = now;

			// Get input
			input();

			// If the delta surpasses 1 then update.
			while (delta >= NS_PER_UPDATE) {

				// Update application elements
				update();

				// Reset the delta
				delta -= NS_PER_UPDATE;

				// Let the application render
				shouldRender = true;

				// Increment update counter
				updates++;
			}

			// Check if the application has updated anything that needs to be
			// rendered.
			if (shouldRender) {

				// Render the updates
				render();

				// Update the frames counter
				frames++;

			}

			// This checks to see if a second has passed
			if (System.currentTimeMillis() - lastTimer >= 1000) {

				// If a second has passed then add another second to it.
				lastTimer += 1000;

				// Run debug methods here
				if (Debug.DEBUG_MODE) {
					debug();
				}

				// Reset frames
				frames = 0;

				// Reset updates
				updates = 0;
			}
		}
	}

	private void debug() {
		message = "Frame: " + frames + ", Update: " + updates;
	}

	private void render() {
		BufferStrategy bs = screen.getBufferStrategy();
		if (bs == null) {
			screen.createBufferStrategy(3);
			return;
		}
		Graphics graphics = bs.getDrawGraphics();
		for (int i = 0; i < objects.size(); i++) {
			objects.get(i).setGraphics(graphics).render();
		}
		if (Debug.DEBUG_MODE) {
			Debug.showFrames(graphics, message);
		}
		graphics.dispose();
		bs.show();
	}

	private void update() {
		for (int i = 0; i < objects.size(); i++) {
			objects.get(i).incrementCount().update();
		}
	}

	private void input() {

	}

	/**
	 * @return the ui
	 */
	public Screen getUi() {
		return screen;
	}

	/**
	 * @param ui
	 *            the ui to set
	 */
	public void setUi(Screen ui) {
		this.screen = ui;
	}
}
