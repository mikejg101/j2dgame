/*
 * Copyright (c) 2015, Michael Goodwin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package j2dgame.main;

import j2dgame.gamewindow.GameWindow;

/**
 *
 * @author Michael Goodwin
 */
public class Game implements Runnable {

    private boolean isRunning = false;
    private final boolean debug;
    private final double NS_PER_UPDATE = Math.pow(10, 9) / 60D;
    private final GameWindow gameWindow;

    public Game(int width, int height,boolean debug) {
        this.debug = debug;
        gameWindow = new GameWindow(width, height, 6);
        isRunning = true;
    }

    public GameWindow getGameWindow() {
        return gameWindow;
    }

    @Override
    public void run() {
        loop();
    }

    private void loop() {
        long previousTime = System.nanoTime();
        double delta = 0.0;
        int frames = 0;
        int ticks = 0;
        long lastTimer = System.currentTimeMillis();
        while (isRunning) {
            long currentTime = System.nanoTime();
            delta += (currentTime - previousTime);
            previousTime = currentTime;
            input();
            boolean shouldRender = false;
            while (delta >= NS_PER_UPDATE) {
                ticks++;
                update();
                delta -= NS_PER_UPDATE;
                shouldRender = true;
            }
            if (shouldRender) {
                frames++;
                render(delta / NS_PER_UPDATE);
            }
            if ((System.currentTimeMillis() - lastTimer >= 1000) && debug) {
                lastTimer += 1000;
                System.out.println("Frames: " + frames + "/" + "Ticks: " + ticks);
                frames = 0;
                ticks = 0;
            }
        }
    }

    public void stop() {
        isRunning = false;
    }

    private void input() {

    }

    private void update() {
        gameWindow.update();
    }

    private void render(double d) {
        gameWindow.render();
    }

}
